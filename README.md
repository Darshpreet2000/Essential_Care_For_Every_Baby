<div align="center">
<p align="center"><img src="assets/mhbs.png" width="150"></p> 
<h2 align="center"><b>Essential Care For Every Baby</b></h2>
<h4><a href="https://gitlab.com/Darshpreet2000/Essential_Care_For_Every_Baby/-/jobs/1022765811/artifacts/raw/build/app/outputs/flutter-apk/app-release.apk">Download Android Application (Link)</a></h4>
<h4>Or 
use as web app https://essential-care-for-every-baby.web.app/</h4>
</div>

## Goal

To provide clinical decision-support for nurses and doctors delivering essential newborn care interventions during the first day of life. 

This project is developed using Flutter, it supports Android, IOS, Web application with single codebase

## Screenshots

|   |  | |
| ------ | ------ | ------ | 
|<img src="/screenshots/Initial.png"  align="top"> |  <img src="/screenshots/Home.png" align="top">| <img src="/screenshots/List_of_Babies.png" align="top"> |
| Initial Screen  |Home Screen  |List of Babies Screen  
 | <img src="/screenshots/Notification.png" align="top">| <img src="/screenshots/Profile.png" align="top">|<img src="/screenshots/Login.png" align="top">|
||Notification Screen  | Profile Screen | Login Screen|

#### Landscape 

|   |  | 
| ------ | ------ |  
| <img src="/screenshots/Home_Landscape.png" align="top"> | <img src="/screenshots/List_of_Babies_Landscape.png" align="top"> |
| <img src="/screenshots/Notification_Landscape.png" align="top">| <img src="/screenshots/Profile_Landscape.png" align="top">  | 
|<img src="/screenshots/Initial_Landscape.png" align="top">| <img src="/screenshots/Login_Landscape.png" align="top">||

## License

This project is licensed under the Mozilla Public License 2.0 with Healthcare Disclaimer. a copy of this license can be found in `LICENSE`.
