import 'package:flutter/material.dart';
import 'package:newborn_care/screens/notifications/components/notification_app_bar.dart';

class Notifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: NotificationAppBar());
  }
}
